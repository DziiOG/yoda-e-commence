//Context Api
//using this without have to do props drilling
import React, { Component } from 'react'
import {storeProducts, detailProduct} from './data';



const ProductContext = React.createContext();
//Provider
//Consumer

 class ProductProvider extends Component {
     state ={
         products: [],
         detailProduct: detailProduct,
         searchedProducts: [],
         toggleProducts: false,
         cart: [],
         favorite: [],
         modalOpen: false,
         modalProduct: detailProduct,
         cartSubTotal: 0,
         cartTax: 0,
         cartTotal: 0
     }

     componentDidMount(){
         this.setProducts();
     }

     setProducts = () => {
         let tempProducts = [];
         storeProducts.forEach(item => {
             const singleItem = {
                 ...item
             };
             tempProducts = [...tempProducts, singleItem];
         })
         this.setState(()=> {
             return {products: tempProducts};
         })
     }


     //Toggle changes between search and home paths

     handleDetails = (id) => {
         const product = this.getItem(id);
         this.setState(()=> {
             return {detailProduct:product};
         })
     }

     addToCart = (id) => {
         let tempProducts = [...this.state.products];
         const index = tempProducts.indexOf(this.getItem(id));

         const product = tempProducts[index];

         product.inCart = true;
         product.count = 1;
         const price = product.price;
         product.total = price;

         this.setState(()=>{
             return {
                 products: tempProducts,
                 cart: [...this.state.cart, product]
             }
         }, ()=> {
             
             this.addTotals();
         })
     }

     addToFavorite = (id) => {
        let tempProducts = [...this.state.products];
        const index = tempProducts.indexOf(this.getItem(id));

        const product =  tempProducts[index];

        if(product.favorite === false){
            product.favorite = true
        } else {
            product.favorite = false
        }

        this.setState(()=>{
            return{
                products: tempProducts,
                favorite: [...this.state.favorite, product]
            }
        }, ()=> {
            console.log(this.state);
        })


     }

     getItem = (id) => {
        const product = this.state.products.find(item => item.id === id) 
        return product;
     }

     searchStoreProducts = (type) => {
         //console.log(type)
         var array1 = [];
         var array2 = [];
         //var array3 = [];
         //var itemArray = ["Yam eggs", "Yam boals", "yam opd", "egg", "home", "house"]
         

        this.state.products.map((element)=> {
          return  array1.push(element.title.toLowerCase());
        })

        //console.log(array1);

        var results = array1.filter(function(elem){
            return elem.toLowerCase().indexOf(type) > -1;
        })

        //console.log(results)
        results.forEach((item)=> {
            this.state.products.map((element)=>{
                if(item.toLowerCase() === element.title.toLowerCase()){
                    array2.push(element)
                }
            })
        })

        //console.log(array2);

        this.setState({
            toggleProducts:true,
            searchedProducts: array2
        })
       
        //console.log(array3)  //return array2
     }

     backToHome = () => {
         this.setState({
             toggleProducts:false
         })
     }
     

     openModal = id => {
         const product = this.getItem(id);

         this.setState(()=> {
             return {
                 modalProduct:product,
                 modalOpen: true
            
            }
         })
     }

     closeModal = () => {
         this.setState(()=>{
             return {modalOpen: false}
         })
     }

     increment = (id) => {
        let tempCart = [...this.state.cart];
        const selectedProduct = tempCart.find(item => item.id === id)


        const index = tempCart.indexOf(selectedProduct);
        const product = tempCart[index];

        product.count = product.count + 1;
        product.total = product.count * product.price;

        this.setState(()=>{return{cart: [...tempCart]}}, ()=>{
            this.addTotals();
        })
     }

     decrement = (id) => {
        let tempCart = [...this.state.cart];
        const selectedProduct = tempCart.find(item => item.id === id)


        const index = tempCart.indexOf(selectedProduct);
        const product = tempCart[index];

        product.count = product.count - 1;
        if(product.count === 0){
            this.removeItem(id)
        }else {
            product.total = product.count * product.price;
            this.setState(()=>{return{cart: [...tempCart]}}, ()=>{
                this.addTotals();
            })
        }

        
     }

     removeItem = (id) => {
         let tempProducts = [...this.state.products];
         let tempCart = [...this.state.cart];

         tempCart = tempCart.filter(item => item.id !== id);
         const index = tempProducts.indexOf(this.getItem(id));
        let removedProduct = tempProducts[index];
        removedProduct.inCart = false;
        removedProduct.count = 0;
        removedProduct.total = 0;

        this.setState(()=> {
            return {
                cart: [...tempCart],
                products: [...tempProducts]
            }
        }, ()=>{
            this.addTotals();
        })
     }

     clearCart = () => {
         this.setState(()=>{
             return {cart:[]}
         }, ()=>{
             this.setProducts();
             this.addTotals();
         })
     }
     addTotals = () => {
         let subTotal = 0;
         this.state.cart.map(item => (subTotal += item.total));
         const tempTax = subTotal * 0.9;
        const tax = parseFloat(tempTax.toFixed(2));
        const total = subTotal + tax
        this.setState(()=> {
            return{
                cartSubTotal:subTotal,
                cartTax:tax,
                cartTotal: total
            }
        })

     }



    render() {
        //console.log(array3)
        //console.log(this.state.searchedProducts)
        

        return (
            <ProductContext.Provider value={{
                ...this.state,
                handleDetails: this.handleDetails,
                addToCart: this.addToCart,
                searchStoreProducts: this.searchStoreProducts,
                backToHome: this.backToHome,
                addToFavorite: this.addToFavorite,
                openModal: this.openModal,
                closeModal: this.closeModal,
                increment: this.increment,
                decrement: this.decrement,
                removeItem: this.removeItem,
                clearCart: this.clearCart
            }}>

            {
                this.props.children
            }
            </ProductContext.Provider>
        )
    }
}


const ProductConsumer = ProductContext.Consumer;


export {ProductProvider, ProductConsumer}