import React, { Component, Fragment } from 'react';
import {Switch, Route} from 'react-router-dom'
//import logo from './logo.svg';
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css"
import Navbar from './components/Navbar';
import ProductList from './components/ProductList';
import Cart from './components/Cart/Cart';
import Default from './components/Default';
import Details from './components/Details';
import Modal from './components/Modal';



class App extends Component {
  
  
  render(){
    return (
     <Fragment>
          <Navbar></Navbar>
        <Switch>
          <Route exact path="/" component={ProductList}></Route>
          <Route path="/details" component={Details}></Route>
          <Route path="/cart" component={Cart}></Route>
          <Route path="/searchresults" component={ProductList}></Route>  
          <Route  component={Default}></Route>
        </Switch>
        <Modal></Modal>
     </Fragment>
    )
  }
}

export default App;
