import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import SportsEsportsIcon from '@material-ui/icons/SportsEsports';
import SmartphoneIcon from '@material-ui/icons/Smartphone';
import PersonalVideoIcon from '@material-ui/icons/PersonalVideo';
import FaceIcon from '@material-ui/icons/Face';
import FastfoodIcon from '@material-ui/icons/Fastfood';
import KitchenIcon from '@material-ui/icons/Kitchen';

import { Link } from 'react-router-dom';
import clsx from 'clsx';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      textDecoration: "none",
      position: "relative"
    },
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
    position: 'relative'
  },
}));

export default function FloatingActionButtons({handleCategorySelector, back, toggler}) {
  const classes = useStyles();


  

  return (
    <div className={clsx(classes.root, )}>
      <Fab variant="extended" onClick={()=>{handleCategorySelector("")}}>
          All Products
      </Fab>
      <Fab color="primary" aria-label="mobile phones" onClick={()=>{handleCategorySelector("phone")}}>
        <SmartphoneIcon></SmartphoneIcon>
      </Fab>
      <Fab color="secondary" aria-label="electronics"   onClick={()=>{handleCategorySelector("Appliances")}}>
        <KitchenIcon></KitchenIcon>
      </Fab>
      <Fab color="primary" aria-label="Groceries"   onClick={()=>{handleCategorySelector("Grocery item")}}>
        <FastfoodIcon></FastfoodIcon>
      </Fab>
      <Fab color="secondary" aria-label="Personal Care"   onClick={()=>{handleCategorySelector("Personal care")}}>
        <FaceIcon></FaceIcon>
      </Fab>
      <Fab color="primary" aria-label="Gaming"   onClick={()=>{handleCategorySelector("Gaming")}}>
        <SportsEsportsIcon></SportsEsportsIcon>
      </Fab>
      <Fab color="secondary" aria-label="Television and Home Audio sets"  onClick={()=>{handleCategorySelector("Tv and Home Audio sets")}}>
        <PersonalVideoIcon></PersonalVideoIcon>
      </Fab>
      <Fab color="primary" aria-label="fashion"   onClick={()=>{handleCategorySelector("fashion")}}>
        <i className='fas fa-tshirt' ></i>
      </Fab>
      {
        (toggler) &&
      <Link to='/' className="returnHome">
      <Fab variant="extended" onClick={()=>{back()}} >
          Back to All Store Products
      </Fab>
      </Link>
      }
    </div>
  );
}


