import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Slide from '@material-ui/core/Slide';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import FloatingActionButtons from './fab';
import styled from 'styled-components';
import clsx from 'clsx'
import { ProductConsumer } from '../context';

const useStyles = makeStyles((theme) => ({
  root: {
   height: 160,
   width: "auto",
   
  },
  paper: {
      flexDirection: "column",
    '& > *': {
      margin: theme.spacing(1),
      width: theme.spacing(150),
      height: theme.spacing(10),
    },
  },
}));

export default function SimplePaper({handleCategorySelector}) {
  const classes = useStyles();

  const [checked, setChecked] = React.useState(true);

  const handleChange = () => {
    setChecked((prev) => !prev);
  };
  
  return (
    <div className={classes.root}>
      <div className={classes.wrapper}>
        <FormControlLabel
          control={<Switch checked={checked} onChange={handleChange} />}
          label="Show Featured Categories"
        />
        <Slide direction="left" in={checked} mountOnEnter unmountOnExit>
            <PaperWrapper>
                <Paper elevation={0} className={clsx(classes.paper,"fabpaper ", )} square={false}>
                <ProductConsumer>
                  {
                    (value)=>{

                    return <FloatingActionButtons handleCategorySelector={handleCategorySelector} back={value.backToHome} toggler={value.toggleProducts}></FloatingActionButtons>
                    }
                  }
                </ProductConsumer>
                </Paper>
            </PaperWrapper>
            
        </Slide>
      </div>
      
      
    </div>
  );
}


const PaperWrapper = styled.div `
    .fabpaper{
        border-color:transparent;
        transition:all 1s linear;
        background:transparent !important;
        position: relative;
        overflow:auto;
    }
   
`; 