import React from 'react';
import {Link} from 'react-router-dom'
//import logo from '../logo.svg'
import clsx from 'clsx';
import {ButtonContainer, ButtonContainer2} from "./Button"
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Drawer from '@material-ui/core/Drawer';


import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import CustomizedInputBase from './search';
import { ProductConsumer } from '../context';





const classes = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(9),
    },
    title: {
      flexGrow: 1,
    },
    appbar: {
        height: 20,
        width: 50
    },
    list: {
      width: 250,
    },
    fullList: {
      width: 'auto',
    },
    
    
   
  }));

 
 
 

export default function Navbar() {
 
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <div
      className={clsx(classes.list, {
        [classes.fullList]: anchor === 'top' || anchor === 'bottom',
      },)}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >

      <List>
        <ListItem button>
        <Link to="/">
                  <Link to="/" className="drawer-link">
                  <span className="mr-2">
                    <i className="fas fa-cart-plus"></i>
                  </span>
                      Yoda E-commence
                  </Link>
        </Link>
        </ListItem>
      </List>
      <Divider />
      <List>
        {['Home', 'Orders', 'WishList'].map((text, index) => (
          <ListItem button key={text}>
            <ButtonContainer2>
              <Link to={text === 'Home' ? '/' : null}>
                <ListItemText primary={text} />
              </Link>
            </ButtonContainer2>
          </ListItem>
        ))}
      </List>
      <Divider />
      <List>
        {['Settings'].map((text, index) => (
          <ListItem button key={text}>
            <ButtonContainer2>
              <Link>
                  <ListItemText primary={text} />
              </Link>
            </ButtonContainer2>
          </ListItem>
        ))}
      </List>
      <Divider />
      <List>
        {['Sell on Yoda', 'Help'].map((text, index) => (
          <ListItem button key={text}>
            <ButtonContainer2>
              <Link>
                <ListItemText primary={text} />
              </Link>
            </ButtonContainer2>
          </ListItem>
        ))}
      </List>
      <Divider/>
    </div>
  );

        return (
                <div className={classes.root}>
                <AppBar position="static" className="navWrapper">
                    <Toolbar >
                    <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu"  onClick={toggleDrawer('left', true)}>
                        <MenuIcon />
                    </IconButton>
                    <ProductConsumer>
                      {
                        (value)=> {
                          return <CustomizedInputBase search={value.searchStoreProducts} back={value.backToHome}
                          ></CustomizedInputBase>
                        }
                      }
                    </ProductConsumer>
                      
                        
                     
                   <Link to="/cart" className="ml-auto">
                        <ButtonContainer>
                          <span className="mr-2">
                            <i className="fas fa-cart-plus"></i>
                          </span>
                              my cart
                        </ButtonContainer>
                   </Link>
                    </Toolbar>
                </AppBar>
                <div>
                  
                    <React.Fragment key={'left'}>
                      <Drawer anchor={'left'} open={state['left']} onClose={toggleDrawer('left', false)}>
                        {list('left')}
                      </Drawer>
                    </React.Fragment>
                  
                </div>
                </div>
        
        )
    
}

