import React, { Component } from 'react';
import Product from './Product'
import Title from './Title';
import{ProductConsumer} from '../context';
import SimplePaper from './Category';


export default class ProductList extends Component {

    state={
        category: "",
        toggle: false
    }

    categorySelector = (type) => {
        if(type === "phone"){
            return "phone"
        }

        if(type === "fashion"){
            return "fashion"
        }
        if(type === "Gaming"){
            return "Gaming"
        }
        if(type === "Personal care"){
            return "Personal care"
        }
        if(type === "Tv and Home Audio sets"){
            return "Tv and Home Audio sets"
        }
        if(type === "Appliances"){
            return "Appliances"
        }
        if(type === "Grocery item"){
            return "Grocery item"
        }
        if(type === ""){
            return ""
        }
        

    }

    handleCategorySelector = (type) => {
         this.categorySelector(type);

        //console.log(this.categorySelector(type))
        this.setState({
            category: this.categorySelector(type),
            text: "",
            backtoHome: []
        })
    }
   

   

    render() {
        

        return (
            <React.Fragment>
                <div className="py-5">
                    <div className="container">
                        <SimplePaper handleCategorySelector={this.handleCategorySelector}  ></SimplePaper>
                        <div className="row">
                            <ProductConsumer>
                                {(value)=>{
                                    if(value.toggleProducts === false){

                                    return value.products.map((product) => {
                                        if((product.category === this.state.category) || (this.state.category === "")){

                                          return   <Product  key={product.id} product={product}></Product>
                                        }
                                    }) 
                                    } else {
                                        if(value.searchedProducts !== false) {
                                            
                                            
                                        return value.searchedProducts.map((product) => {
                                        if((product.category === this.state.category) || (this.state.category === "")){

                                          return   <Product  key={product.id} product={product}  ></Product>
                                        } 
                                    }) 
                                        } else {
                                            return <Product>
                                                <h5>Nothing Found</h5>
                                            </Product>
                                        }

                                        
                                    }
                                }}

                            </ProductConsumer>
                        </div>
                    </div>
                   
                </div>
            </React.Fragment>


              // <Product></Product>
            
        )
    }
}
