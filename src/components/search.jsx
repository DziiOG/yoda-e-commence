import React, {useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import clsx from 'clsx'
import {Link} from 'react-router-dom'




const useStyles = makeStyles((theme) => ({
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 200,
    marginLeft: 10
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 1,
  },
  divider: {
    height: 28,
    margin: 4,
  },
}));

export default function CustomizedInputBase({search}) {
  const classes = useStyles();
  const [state, setState] = React.useState({
   text: ""
  });

  const toggleTyping = (event) => {
   
    setState({ text: event.target.value });
    //console.log(state['text'])
  };

 
  

  return (
    
      <Paper component="form" className={clsx(classes.root, "paper-search")}>
        
        <InputBase
          className={clsx(classes.input)}
          placeholder="Search Yoda Store"
          inputProps={{ 'aria-label': 'search google maps' }}
          onChange={(event)=>{toggleTyping(event)}}
          onKeyPress={(event) => { if(event.keyCode === 13){
           search(state['text']);
           alert("alert")
        }}}
        
        />
        <Link to="/searchresults">
        <IconButton  onClick={()=> {search(state['text'])}} className={classes.iconButton} aria-label="search">
          <SearchIcon />
        </IconButton>
        </Link>
        <Divider className={classes.divider} orientation="vertical" />
      </Paper>
    
  );
}


